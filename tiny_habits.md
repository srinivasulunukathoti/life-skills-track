## In this video, what was the most interesting story or idea for you?

starting with small tasks can make it easier to overcome initial hurdles and get started,rather than tackling a massive take all at once


Adding tiny habbibts to your routine makes it more likely that you will stick with them over time


The success of small habits can be motivating, leading to a sense of accomplishment and encouraging further progress.

## How can you use B = MAP to make making new habits easier? What are M, A and P.
The B=MAP model helps you understand the key ingredients for establishning new habits.

- B = Behavior: This is the new habit you want to create, like flossing daily.

- M = Motivation: Your desire and ability to perform the behavior.

- Ability: Feeling confident you can do it .

- Prompt: A trigger to remind you (phone reminder to floss).


## Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)

- positive experiences create a strong mental link with the habit

- it reminds you of your abilities and fuels motivation.

- It triggers positive emotions, making the habit more enjoyable.

## what was the most interesting story or idea for you?
it explain that concentrating only on achieving distant or ambitious goals can be discourage.Instead of ,it highlights the value of developing habits that gratually move to you towords your goals.By focusing on daily,small improvements,you create momentum and sustainable progress

## What is the book's perspective about Identity?
- The book advocates a shift from outcome-focused thinking to identity-focused thinking. Rather than setting goals based on desired outcomes, such as weight loss, the book suggests focusing on one's identity. 

- This means adopting an identity statement like "I am a healthy person" and then prioritizing habits that align with this identity. By emphasizing identity and aligning habits accordingly, the book argues for a more effective and sustainable approach to personal growth

## Write about the book's perspective on how to make a habit easier to do?

- Make the cues obvious. This means designing your environment in a way that will trigger the desired behavior. 
- Instead of just aiming for a big win (goal), focus on the daily routines (system) that will get you there. Think less "lose weight" and more "healthy habits."


## Write about the book's perspective on how to make a habit harder to do?

- Find ways to make the unwanted habit less appealing. Pair it with something unpleasant or inconvenient.

- Increase the friction associated with the bad habit. This could involve making it physically harder to do or adding extra steps. 

## Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- Start with shorter workout sessions and gradually increase intensity and duration.


## Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

- Remove social media apps from the home screen of my phone or hide them in a folder.

- Use website blockers or productivity apps to restrict access to social media during certain hours.
- Keep devices out of reach or in another room during focused work or leisure activities


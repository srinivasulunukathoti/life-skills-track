# DOMAIN DRIVEN DESIGN
 - Domain-Driven Design (DDD) is an approach to software development that focuses on understanding and modeling the problem domain within which a software system operates.
 
 - DDD provides a set of principles, patterns, and practices to help developers effectively capture and express domain concepts in their software designs.
[]![alt text](image.png)

 **Domain-driven design is predicated on the following goals:**

    
    
- placing the project's primary focus on the core domain and domain logic.
- basing complex designs on a model of the domain.
- initiating a creative collaboration between technical and domain experts to iteratively refine a conceptual model that addresses particular domain problems.

**Benefits of Domain-Driven Design(DDD)?**

- It encourages collaboration between domain experts, developers, and stakeholders. 
- By encouraging a shared understanding of the problem domain through the ubiquitous language, teams can communicate more effectively and ensure that the software accurately reflects the needs and requirements of the business.

# OVERVIEW

Domain-driven design articulates a number of high-level concepts and practices.[4]

Of primary importance is a domain of the software, the subject area to which the user applies a program. Software's developers build a domain model: a system of abstractions that describes selected aspects of a domain and can be used to solve problems related to that domain.

These aspects of domain-driven design aim to foster a common language shared by domain experts, users, and developers—the ubiquitous language. The ubiquitous language is used in the domain model and for describing system requirements.

Ubiquitous language is one of the pillars of DDD together with strategic design and tactical design.

In domain-driven design, the domain layer is one of the common layers in an object-oriented multilayered architecture. 
# References

Haywood, Dan (2009), [Domain-Driven Design using Naked Object](https://pragprog.com/titles/dhnako/domain-driven-design-using-naked-objects), Pragmatic Programmers.
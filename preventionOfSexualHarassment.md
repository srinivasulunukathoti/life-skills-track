# Sexual Harassment: Recognizing Different Forms

Sexual harassment comes in various forms, and it's important to be aware of them. Here's a breakdown of the three main types:

## Verbal

- This includes unwelcome comments about a person's appearance, sexual jokes, innuendos, requests for sexual favors, and vulgar messages.

## Visual

- This involves unwanted visual content of a sexual nature, such as:
    - Drawing insulting pictures of someone
    - Sending sexually suggestive images or videos
    - Editing someone's photos in a harassing way

## Physical

- This is unwanted physical contact, including:
    - Touching
    - Kissing
    - Hugging
    - Blocking someone's path
    - Rubbing against someone

# What to Do If You Experience Sexual Harassment

1. **Report it:** Tell your supervisor or a trusted person about the harassment.
2. **Document it:** Keep a record of the incidents, including dates, times, and details of what happened.
3. **Seek Help:** If your supervisor doesn't take action, consider filing a complaint with the Human Resources department or the authorities.

Remember: You don't have to tolerate sexual harassment. There are resources available to help you.

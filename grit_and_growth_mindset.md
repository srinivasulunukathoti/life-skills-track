## 1.Introduction of Grit?
The video talks about the importance of grit, which means never giving up and staying determined. It says that even if you're not naturally talented, if you keep working hard and staying passionate about what you're doing, you can still succeed.

## 2. Introduction to Growth Mindset?
In the video, they explain the idea of a growth mindset, which means believing that you can improve your abilities through effort. They contrast this with a fixed mindset, where people think their skills are fixed and can't change.


## 3.What is the Internal Locus of Control? What is the key point in the video?
The Internal Locus of Control means believing that you control what happens to you, rather than thinking it's all up to luck or outside forces.

The main point in the video is probably about how important it is to realize that you have control over your own life and to use that belief to stay motivated and focused on your goals.

## 4.What are the key points mentioned by speaker to build growth mindset?
- Believe in yourself.
- Question yourself where you're struct.
- Try to learn new things.
- Accept the challenges, do not quit.
- Stay motivated.
- Focus on your goal.

## 5.What are your ideas to take action and build Growth Mindset?
- I will focus more on writing clear and understandable code.
- I will set clear coding goals for myself.
- I will actively seek feedback from mentors to gain insights and improve my skills.
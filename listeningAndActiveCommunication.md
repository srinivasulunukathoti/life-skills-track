## What are the steps/strategies to do Active Listening?
- **Concentrate:** Put your attention entirely on the person speaking. Ignore other things that might distract you.
- **Listen with Your Eyes and Ears:** Use your eyes to watch the speaker and your ears to hear what they're saying.This helps you catch all the important details.
- **Stay Present:** Try not to let your mind wander. Stay in the moment and give the speaker your full focus.
- **Avoid Multi-Tasking:** Don't try to do other things while you're listening. Give the speaker your undivided attention.
- **Engage Your Brain:** Actively think about what the speaker is saying.Ask yourself questions to make sure you  understand.
- **Take Notes (if needed):** If it helps you stay focused, jot down key points as the speaker talks.
- **Practice being patient:** Listening well takes practice. Keep trying and be patient with yourself and others. You'll get better with time.

## According to Fisher's model, what are the key points of Reflective Listening?
- **Understanding Feelings:** Reflecting the other person's emotions.
- **Confirming Understanding:** Making sure you got the message right.
- **Helpful in Serious Situations:** Useful in important talks.
- **Taking Good Notes:** Writing down what's said in meetings.
- **Calming Heated Talks:** Helps calm emotional conversations.
- **Clearing Tech Discussions:** Keeping technical talks clear with notes.

## What are the obstacles in your listening process?
If i am not really interested in what's being talked about or who's talking, I might get distracted and have trouble paying attention. This can make it tricky to understand what's going on in the conversation.
## What can you do to improve your listening?

Pay attention to the conversation and try not to let your mind wander to other thoughts or daydreams.

## When do you switch to Passive communication style in your day to day life?

 When I feel uncomfortable expressing my needs or opinions, or when I want to avoid conflict, I tend to switch to a passive communication style.

## When do you switch into Aggressive communication styles in your day to day life?

I usually switch to an aggressive communication style when I feel frustrated, threatened, or disregarded, and I express my opinions forcefully.

## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

When I'm unable to address issues directly, I might resort to passive-aggressive communication styles like giving the silent treatment to express my dissatisfaction indirectly.

## How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life?

To improve my communication and be more assertive, I watch videos on assertive communication techniques and practice steps like expressing myself clearly, using 'I' statements, and setting boundaries in my daily interactions.